package com.hepisc.elkwebflux.controller;

import com.hepisc.elkwebflux.model.Department;
import com.hepisc.elkwebflux.model.Employee;
import com.hepisc.elkwebflux.model.Organization;
import com.hepisc.elkwebflux.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping
    public Mono<Employee> add(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping("/{name}")
    public Flux<Employee> findByName(@PathVariable("name") String name) {
        return employeeRepository.findByName(name);
    }

    @GetMapping
    public Flux<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @GetMapping("/count/all")
    public Mono<Long> count() {
        return employeeRepository.count();
    }

    @GetMapping("/organization/{organizationName}")
    public Flux<Employee> findByOrganizationName(@PathVariable("organizationName") String organizationName) {
        return employeeRepository.findByOrganizationName(organizationName);
    }

    @PostMapping("/generate")
    public Flux<Employee> generateMulti() {
        return employeeRepository.saveAll(employees()).doOnNext(employee -> LOGGER.info("Added: {}", employee));
    }

    private List<Employee> employees() {
        List<Employee> employees = new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            Random r = new Random();
            Employee employee = new Employee();
            employee.setName("JohnSmith" + r.nextInt(1000000));
            employee.setAge(r.nextInt(100));
            employee.setPosition("Developer");
            int departmentId = r.nextInt(5000);
            employee.setDepartment(new Department((long) departmentId, "TestD" + departmentId));
            int organizationId = departmentId % 100;
            employee.setOrganization(new Organization((long) organizationId, "TestO" + organizationId, "Test Street No. " + organizationId));
            employees.add(employee);
        }
        return employees;
    }
}
